/* 
    Document   : lib
    Author     : Chrástek Tomáš, Klape s.r.o.
    Description:
        Obsahuje javascripty pro projekt Targin
*/

var Targin = {};

Targin = {
	// Overlay selector
	overlaySelector: "#overlay",
	// Globalni inicializace
	initialize: function(){
		// Reference sama na sebe
		obj = this;
		
		
		// Otevreni MOA popupu
		$("#moa .open-popup").click(function(){
			obj.showOverlay();
			$("#moa .popup").show();
		});
		// Otevreni MOA popupu
		$("#moa .close-popup").click(function(){
			$("#moa .box1" ).hide();
			$("#moa .box2" ).show();
			$("#moa .popup").hide();
			obj.hideOverlay();
		});
		
		// Otevreni pdf
		$(".ref").click(function(){
			$(".ref-box").toggle();
		});
		
		obj.initCounterWithAnimation();
	},
	initCounterWithAnimation: function(){
		fingerprintClickCounter = 0;
		$("#moa .fingerprint").unbind();
		$("#moa .fingerprint").click(function(){
			fingerprintClickCounter++;
			$(".popup .dynamic-content div").hide();
			switch(fingerprintClickCounter){
				case 1: 
					$(".popup .animation2").show();
					$(".text2").show();
					break;
				case 2:
					$(".popup .animation3").show();
					$(".text3").show();
					break;				
				case 3:
					$(".fingerprint").hide();
					$(".popup .animation4").show();
					$(".text4").show();
					break;				
			}
		});	
	},
	showOverlay: function(){
		$(obj.overlaySelector).show();
	},
	hideOverlay: function(){
		$(obj.overlaySelector).hide();
	},
	reset: function(){
		$(".popup .dynamic-content div").show();
		$(".text1").show();
		$(".popup .animation1").show();
		$(".text2").hide();
		$(".popup .animation2").hide();
		$(".text3").hide();
		$(".popup .animation3").hide();
		$(".text4").hide();
		$(".popup .animation4").hide();
		$(".fingerprint").show();
		$(".ref-box").hide();
		$("#moa .box1" ).show();
		$("#moa .box2" ).hide();
		$("#moa .popup").hide();
		this.hideOverlay();
		this.initCounterWithAnimation()
	}
};